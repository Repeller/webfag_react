import React from 'react';

import { Route, Switch, Link } from 'react-router-dom'

import Home from './Home'
import Page2 from './Page2'

const App = () => (
    <div>
        <Link to='/'>home</Link><br></br>
        <Link to='/page2'>Page2</Link><br></br>   
        <Link to='/page2/6'>Page + 6</Link><br></br>   

    
        <Route exact path='/' component={Home} />
        <Route path='/page2' component={Page2} />
    </div>
)

export default App
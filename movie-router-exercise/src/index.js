import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import App from './components/App';
import reducers from './reducers';
import { BrowserRouter } from 'react-router-dom';


ReactDOM.render(
    <BrowserRouter>
    
    <Provider store={createStore(reducers)}>
        <App />
    </Provider>
    </BrowserRouter>,
     document.getElementById('root')
);


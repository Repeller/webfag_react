import React, { Component } from 'react';
import MovieList from './MovieList';
import MovieDetail from './MovieDetail';
import { Route, Switch } from 'react-router-dom';

class Main extends Component {

  // <Switch>
  //   <Route exact path='/' Component={Movies} />
  //   <Route path='/About' Component={About} />
  //   <Route path='/Login' Component={UnderConstruction} />
  //   <Route path='/Admin' Component={UnderConstruction} />
  //   <Route path='/MovieDetails' Component={MovieDetails} />
  // </Switch>

  render() {
      return (
          
          <div>
            <div className="row">
                <div className="col-sm-1"></div>
                <div className="col-sm-4">
                    <MovieList />
                </div>
                <div className="col-sm-7"></div>       
            </div>
            <div>
                <MovieDetail/>
            </div>  
            
        </div>
      );
    }
  }
  
  export default Main;

    
import React, { Component } from 'react';
import MovieImg from '../assets/Image/movie_black2.jpg';
import MovieList from './MovieList';
import MovieDetail from './MovieDetail';
import { BrowserRouter, Link } from 'react-router-dom';


class Header extends Component {
  
  
  render() {
    return (
      <div className="Header">
        <div className="jumbotron">
          <ul className="list-group">
            <li className="list-group-item">
              <Link to='/'>home</Link>
            </li>
            <li className="list-group-item">
              <Link to='/Admin'>Admin</Link>
            </li>
            <li className="list-group-item">
              <Link to='/About'>About</Link>
            </li>
            <li className="list-group-item">
              <Link to='/Login'>Login</Link>
            </li>
          </ul>
          <h1>React-Redux-Router Movies  <img alt="Movie" className ="rounded" src={MovieImg} style={{width:100, height:100}}></img></h1>  
           This small React-App demonstrates the use of React-Router
        </div> 
        </div>
    );
  }
}

export default Header;
import React from 'react';
import { connect } from 'react-redux';
import { increment, decrement } from '../actions';

// I need to get the value, so I can give it to the 'Counter'
//const currentNum = store.count;

const Counter = (props) => {
    return (
        <div>
            <button className="ui button primary"
            onClick={props.increment}>Increment</button>
            <button className="ui button primary"
            onClick={props.decrement}>Decrement</button>
            {/* I want to pass the value into that span tag */}
    Current count: <span>{props.countProp}</span>
        </div>
    );
};

// all below this was added
const mapStateToProps = (state) => {
    return {countProp : state.count};
};


export default connect(mapStateToProps, {increment, decrement})(Counter);



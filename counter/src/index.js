// this file controls everything

// import all you need
import React from 'react';
import ReactDOM from 'react-dom';
// needed to run redux funktion and everything else too
import { Provider } from 'react-redux';
import { createStore } from 'redux';
// import the components
import App from './components/App';
import countReducer from './reducers';

const testElement = <p>test</p>;

// these are used to render everything to page
ReactDOM.render(
    // The <Provider /> makes the Redux store available to any nested  
    // components that have been wrapped in the connect() function.

                    // create store, make the store with the 'countReducer'
    <Provider store={createStore(countReducer)}>
        {/* runs the 'app' part  */}
        <App />
    </Provider>,  
    document.getElementById('root'));
// you can run more than one render in the same script
ReactDOM.render(
    testElement,document.getElementById('root2'));



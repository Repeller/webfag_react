import React, {Component} from 'react';

class Clock extends Component {
    // runs when created
    // I never gave it the props, so it most get it by self
    constructor(props) {
        super(props);
        this.state = {date: new Date()};
    }
    // gets called after the componted have been mounted
    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }
    // when we unmount it, we clear this/these values
    componentWillUnmount() {
        clearInterval(this.timerID)
    }
    // the function we call every secound
    tick() {
        this.setState({
            date: new Date()
        });
    }

    // this will be the content, that we return and show on the page
    render() {
        return (
            <div>
                <h2>
                    the Clock is {this.state.date.toLocaleTimeString()}.
                </h2>
            </div>
        );
    }
}

export default Clock;
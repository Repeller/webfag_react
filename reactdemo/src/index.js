// import of React libraries
import React from 'react';
import ReactDOM from 'react-dom';


const helloText = "whats up bitches!!!!";
// create a react component
const App = () => {
    //return <div>Hello world!</div>;
    // this is not HTML, but JSX
    return <div style={{background: 'green', color: 'white'}}>
        <h1>{helloText}</h1>
    </div>;
}

// render the react component in the browser
// you can see that, we call the function/component that we called 'App'
// JSX is not HTML!

ReactDOM.render(<App/>, 
    document.getElementById('root') );

// ReactDOM.render(<App/>, 
//     document.querySelector('#root') );



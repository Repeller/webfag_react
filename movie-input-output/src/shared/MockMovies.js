// the data of the moives

import axios, {
    AxiosResponse,
    AxiosError
} from "../../node_modules/axios/index"; // Don't worry about red lines here ...

const baseUrl = 'https://api.themoviedb.org/3/movie/';

var justMovies = [];
const getMovies = () => {
    var res = axios.get(('https://api.themoviedb.org/3/movie/550?api_key=5963640c8ffeb74721e37ed1f6eb9c56') );
    console.log('res', res);
    console.log('res', res.data);
    console.log('res', res.AxiosResponse);
    console.log('res', res.AxiosResponse.data);



    
    return res;
};

// justMovies.push(await axios.get((baseUrl+'2355?api_key=5963640c8ffeb74721e37ed1f6eb9c56') ));
// justMovies.push(await axios.get((baseUrl+'2385?api_key=5963640c8ffeb74721e37ed1f6eb9c56') ));
// justMovies.push(await axios.get((baseUrl+'42132?api_key=5963640c8ffeb74721e37ed1f6eb9c56') ));
// justMovies.push(await axios.get((baseUrl+'200?api_key=5963640c8ffeb74721e37ed1f6eb9c56') ));

var MOCK_MOVIES = justMovies.map((x) => {
    var temp = {};
    temp.Title = x.title;
    temp.Year = x.release_date;
    temp.Genre = x.genres;
    temp.Actors = "we don't know";
    temp.Plot = x.overview;
    temp.Poster = x.backdrop_path;
    return temp;
});
console.log('justMovies',justMovies);
console.log('getMovies',getMovies);

console.log('getMovies',getMovies.data);
console.log('map movies',MOCK_MOVIES);

// const MOCK_MOVIES = [
// {
//     Title:"Furious 7",
//     Year:2015,
//     Genre:"Action, Crime, Thriller",
//     Actors:"Vin Diesel, Paul Walker, Jason Statham, Michelle Rodriguez",
//     Plot:"Deckard Shaw seeks revenge against Dominic Toretto and his family for his comatose brother.", 
//     Poster: "assets/Image/Furious7.jpg"
// },
// {
//     Title:"Batman",
//     Year:1989,
//     Genre:"Action, Adventure",
//     Actors:"Michael Keaton, Jack Nicholson, Kim Basinger, Robert Wuhl",
//     Plot:"The Dark Knight of Gotham City begins his war on crime with his first major enemy being the clownishly homicidal Joker.", 
//     Poster: "assets/Image/Batman.jpg"
// },
// {
//     Title:"Skyfall",
//     Year:2012,
//     Genre:"Action, Adventure, Thriller",
//     Actors:"Daniel Craig, Judi Dench, Javier Bardem, Ralph Fiennes",
//     Plot:"Bond's loyalty to M is tested when her past comes back to haunt her. Whilst MI6 comes under attack, 007 must track down and destroy the threat, no matter how personal the cost.", 
//     Poster: "assets/Image/Skyfall.jpg"
// },
// {
// 	Title:"Avatar",
// 	Year:2009,
// 	Genre:"Action, Adventure, Fantasy",
// 	Actors:"Sam Worthington, Zoe Saldana, Sigourney Weaver, Stephen Lang",
// 	Plot:"A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home.",
// 	Poster: "assets/Image/Avatar.jpg"
// },
// {
// 	Title:"Kill Bill: Vol. 1",
// 	Year:2003,
// 	Genre:"Action, Crime, Thriller",
// 	Actors:"Uma Thurman, Lucy Liu, Vivica A. Fox, Daryl Hannah",
// 	Plot:"The Bride wakens from a four-year coma. The child she carried in her womb is gone. Now she must wreak vengeance on the team of assassins who betrayed her - a team she was once part of.",
// 	Poster:"assets/Image/KillBill.jpg"
// }

// ];

export default MOCK_MOVIES;
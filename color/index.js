//import React from 'react';
//import ReactDOM from 'react-dom';
//import Redux from 'redux';

// action creators
// the keyword 'type' can not be repalced by anything else
const green = () => ({type: 'green'});
const red = () => ({type: 'red'});
const blue = () => ({type: 'blue'});

//reducers
// we start by giving 'color' a start value
// which will only happen ones
const colorReducer = (color="red", action) => {
    if (action.type === 'green') {return "ui label massive green"; }
    if (action.type === 'blue') {return "ui label massive blue"; }
    if (action.type === 'red') {return "ui label massive red"; }
    return color;
};

// store
const store = Redux.createStore(colorReducer);

// update the html page
function render(){
    document.getElementById("labelFace").className = store.getState().toString();
}

//Subscribe på ændringer, dvs callback-funktionen render() kaldes når store opdateres
//everytime the store changes, it will call the render function. This will update the display page
store.subscribe(render);